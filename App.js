import React from 'react';
import Acceuil from './Vue/Acceuil'
import LoginPage from './Vue/LoginPage';
import RegisterPage from './Vue/RegisterPage';
import {StackNavigator} from 'react-navigation';

const NavigationApp = StackNavigator ({
  Home: {screen: Acceuil},
  Login: {screen: LoginPage},
  Register: {screen: RegisterPage},
});

export default class App extends React.Component {
  render() {
    /*return (
    <Acceuil/>
    ) */
    return<NavigationApp/>;
  }
}
