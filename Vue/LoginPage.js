import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import { Ionicons,FontAwesome } from "@expo/vector-icons"
import { render } from 'react-dom';

export default class LoginPage extends React.Component {
/*
    _onPressBtnLogin() {
        this.props.navigation.navigate('Register')
    }
    _onPressBtnRegister() {
        this.props.navigation.navigate('Register')
    }
 */   

static navigationOptions = {
    title: 'Login',
}

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <FontAwesome onPress={()=>navigate('Acceuil')} style={styles.topIcon} name="angle-left" size={50} color='#ff5a66' padding={5}/>
                </View>
                <View >
                    <Text style={styles.text}>Sign In</Text>
                </View>
                <View style={styles.btnContainer}>
                    <TextInput style={styles.input}
                        placeholder = "E-mail or phone number"
                        returnKeyType = "next"
                        onSubmitEditing = {() => this.passwordInput.focus()}
                        keyboardType = "email-address"
                        autoCapitalize = "none"
                        autoCorrect = {false}
                    />
                    <TextInput style={styles.input}
                        placeholder = "Password"
                        returnKeyType = "next"
                        securekeyEntry = {true}
                        ref = {(input) => this.passwordInput = input}
                    />
                    <TouchableOpacity style={styles.btn1} onPress={()=>navigate('Acceuil')} >
                        <Text style={styles.btnText1}>Login</Text>
                    </TouchableOpacity>

                    <Text style={styles.text2}>Or</Text>

                    <TouchableOpacity style={styles.btn2}  onPress={()=>navigate('Acceuil')}>
                        <Text style={styles.btnText2}>Login with Facebook</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        //alignItems: 'center',
        justifyContent: 'flex-start',
    },

    top: {
        backgroundColor: '#fff',
        borderEndColor: 'gray',
        borderWidth:0.4,
        width:415,
        padding:10,
        margin:0,
    },

    topIcon:{
        paddingTop:15,
        marginLeft: 5
    },
    
    text: {
        fontSize: 40,
        color: '#ff5a66',
        margin:5,
        padding: 10
    },

    btnContainer: {
        margin: 30,
        justifyContent: 'space-around',
        alignItems: 'center'
    },

    input: {
        height:50,
        width:300,
        backgroundColor: '#fff',
        fontSize: 18,
        borderWidth:0.5,
        borderColor: 'red',
        alignItems:'center',
        justifyContent:'center',
        padding:15,
        margin:10,
        borderRadius:50
    },

    btn1: {
        width: 250,
        height: 50,
        backgroundColor: '#ff5a66',
        borderColor: '#ff5a66',
        borderWidth:1,
        borderRadius: 50,
        margin: 25,
    },

    text2: {
        textAlign: 'center',
        //fontStyle: 'bold',
        fontSize: 20,
        color: 'black',
        margin:10,
        padding: 15
    },


    btnText1: {
        fontSize: 20,
        color: '#fff',
        textAlign: 'center',padding: 10
    },

    btn2: {
        width: 250,
        height: 50,
        alignItems: 'center',
        backgroundColor: '#4267b2',
        borderRadius: 50,
        margin: 10,
    },
    
    btnText2: {
        fontSize: 20,
        color: '#fff',
        textAlign: 'center',
        padding: 10
    },


});