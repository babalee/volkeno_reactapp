import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import { FontAwesome } from "@expo/vector-icons";

export default class RegisterPage extends React.Component {

    _onPressButtn() {
        this.props.navigation.navigate('Home')
    }
    static navigationOptions = {
        title: 'Register',
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <FontAwesome onPress={()=>navigate('Acceuil')} style={styles.topIcon} name="angle-left" size={50} color='#ff5a66' padding={5}/>
                </View>
                <View >
                    <Text style={styles.text}>Create new account</Text>
                </View>
                <View style={styles.btnContainer}>
                    <TextInput style={styles.input}
                        placeholder = "Full name"
                        returnKeyType = "next"
                        onSubmitEditing = {() => this.phoneNumberInput.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder = "Phone Number"
                        returnKeyType = "next"
                        onSubmitEditing = {() => this.emailInput.focus()}
                        keyboardType = "numeric"
                        ref = {(input) => this.phoneNumberInput = input}
                    />
                    <TextInput style={styles.input}
                        placeholder = "E-mail Address"
                        returnKeyType = "next"
                        onSubmitEditing = {() => this.passwordInput.focus()}
                        keyboardType = "email-address"
                        autoCapitalize = "none"
                        autoCorrect = {false}
                        ref = {(input) => this.emailInput = input}
                    />
                    <TextInput style={styles.input}
                        placeholder = "Password"
                        returnKeyType = "next"
                        securekeyEntry = {true}
                        ref = {(input) => this.passwordInput = input}
                    />
                    <TouchableOpacity style={styles.btn1} onPress={()=>navigate('Acceuil')} >
                        <Text style={styles.btnText1}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        //alignItems: 'center',
        justifyContent: 'flex-start',
    },

    top: {
        backgroundColor: '#fff',
        borderEndColor: 'gray',
        borderWidth:0.4,
        width:415,
        padding:10,
        margin:0,
    },

    topIcon:{
        paddingTop:15,
        marginLeft: 5
    },
    
    text: {
        fontSize: 35,
        color: '#ff5a66',
        margin:5,
        padding: 10
    },

    btnContainer: {
        margin: 30,
        justifyContent: 'space-around',
        alignItems: 'center'
    },

    input: {
        height:55,
        width: 300,
        backgroundColor: '#fff',
        fontSize: 18,
        borderWidth:0.5,
        borderColor: 'gray',
        alignItems:'center',
        justifyContent:'center',
        padding:15,
        margin:10,
        borderRadius:50
    },

    btn1: {
        width: 250,
        height: 50,
        backgroundColor: '#ff5a66',
        borderColor: '#ff5a66',
        borderWidth:1,
        borderRadius: 50,
        margin: 25,
    },

    text2: {
        textAlign: 'center',
        //fontStyle: 'bold',
        fontSize: 20,
        color: 'black',
        margin:30,
        padding: 15
    },
 
    btnText1: {
        fontSize: 20,
        color: '#fff',
        textAlign: 'center',padding: 10
    },
 
});