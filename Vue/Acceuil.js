import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Ionicons,FontAwesome } from "@expo/vector-icons"
import { render } from 'react-dom';

export default class Acceuil extends React.Component {

    /*_onPressBtnLogin() {
        this.props.navigation.navigate('Login')
    }
    _onPressBtnRegister() {
        this.props.navigation.navigate('Register')
    }
*/
    static navigationOptions = {
        title: 'Home',
    }
    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <View >
                    <Text style={styles.text}>Say hello to your new app</Text>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.btn1} onPress={()=>navigate('Login')} >
                        <Text style={styles.btnText1}>Se Connecter</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn2}  onPress={()=>navigate('Register')}>
                        <Text style={styles.btnText2}>S'inscrire</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },

    text: {
        textAlign: 'center',
        fontSize: 40,
        color: '#ff5a66',
    },

    btnContainer: {
        margin: 30,
        justifyContent: 'space-between',
    },

    btn1: {
        width: 290,
        height: 50,
        alignItems: 'center',
        backgroundColor: '#ff5a66',
        borderColor: '#ff5a66',
        borderWidth:1,
        borderRadius: 50,
        margin: 10,
    },
    
    btnText1: {
        fontSize: 20,
        color: '#fff',
        textAlign: 'center',padding: 10
    },

    btn2: {
        width: 290,
        height: 50,
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#ff5a66',
        borderWidth:1,
        borderRadius: 50,
        margin: 10,
    },
    
    btnText2: {
        fontSize: 20,
        color: '#ff5a66',
        textAlign: 'center',
        padding: 10
    },


});